;Test Program

include Irvine32.inc

.data
msgHello BYTE "Hello World",0H

.code
main proc

	call ClrScr
	call Crlf
	call Crlf
	
	mov edx, OFFSET msgHello;
	call WriteString

	call Crlf
	call Crlf


	exit
main endp
end main

	